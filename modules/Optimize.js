const remote = window.require('electron').remote;
const crypto = window.require('crypto');
const path = window.require('path');
var fs = window.require('fs');
const app = remote.getGlobal('app');
const BrowserWindow = remote.getGlobal('BrowserWindow');
const SZip = window.require('node-7z');
const tarball = window.require('tarball-extract')
const Unrar = window.require('node-unrar');
const AdmZip = window.require('adm-zip');
var archive = window.require('archiver');
const copy = window.require('recursive-copy');
var minify = window.require('minify');
const imagemin =  window.require('imagemin');
const imageminPngcrush =  window.require('imagemin-pngcrush');
const imageminJpegtran =  window.require('imagemin-jpegtran');

class Optimize  {

    /**
     *
     * @param files
     * @param success_callback
     * @param error_callback
     */
    constructor(files, success_callback, error_callback) {
        this.files = files || []
        this.success_callback = success_callback || function(){}
        this.error_callback = error_callback || function(){}
        this.tmp_dir = ''
        this.tmp_files = ''
        this.archives = []
        this.op_files = []
        this.optimize()
    }

    /**
     *
     * @param sc
     */
    generateArchive(sc) {
        var _self = this
        //this.archiveExtractedFiles(function(){
        _self.archiveTmpFolder(function(file){
            sc(file)
        })
        //})
    }

    /**
     *
     * @param sc
     */
    archiveTmpFolder(sc){
        var el_path = this.tmp_dir
        var name = path.basename(el_path)
        var dwl_dir = app.getPath('temp')
        var new_path = path.join(dwl_dir, name)
        var _self = this
        if(this.files.length == 1 && this.archives.length == 1){
            var archiveFile = this.archives[0];
            var archiveFile  = archiveFile.replace(/_[^_]+$/, "")
            archiveFile  = archiveFile.replace(".tar", "")
            sc(archiveFile)
        }else {
            this.zipFolder(el_path+'/./',  new_path , function(){
                _self.rmdirAsync(el_path, function(){
                    sc(new_path)
                });
            });
        }

    }

    /**
     *
     * @param source
     * @param dest
     * @param cb
     */
    zipFolder(source, dest , cb){
        dest = dest.replace(".zip", "")
        dest = dest+".zip"
        let ar = archive.create('zip',{});
        let outputStream = fs.createWriteStream(dest, {flags: 'w'});
        outputStream.on('close', function () {
            cb()
        });
        ar.pipe(outputStream);
        ar.directory(source, '/').finalize();
    }

    /**
     *
     * @param sc
     */
    archiveExtractedFiles(sc) {
        var archives = this.archives
        var count = archives.length
        var archived = 0
        var names = []
        var _self = this

        function archiveFolder(i){
            var el_path = archives[i];
            var new_path  = el_path.replace(/_[^_]+$/, "")
            new_path  = new_path.replace(".tar", "")
            if(names.indexOf(new_path+".zip") >=0){
                new_path = new_path+'_'+i
            }
            new_path = new_path+".zip"
            names.push(new_path)
            _self.zipFolder(el_path+'/./', new_path, function () {
                _self.rmdirAsync(el_path, function(){
                    archived++
                    if(archived == count){
                        sc()
                    }else {
                        archiveFolder(archived)
                    }
                })
            })
        }

        if(count > 0){
            archiveFolder(0)
        }else  {
            sc()
        }
    }

    /**
     *
     * @param path
     * @param callback
     */
    rmdirAsync(path, callback) {
        var _self = this
        fs.readdir(path, function(err, files) {
            if(err) {
                // Pass the error on to callback
                callback(err, []);
                return;
            }
            var wait = files.length,
                count = 0,
                folderDone = function(err) {
                    count++;
                    // If we cleaned out all the files, continue
                    if( count >= wait || err) {
                        fs.rmdir(path,callback);
                    }
                };
            // Empty directory to bail early
            if(!wait) {
                folderDone();
                return;
            }

            // Remove one or more trailing slash to keep from doubling up
            path = path.replace(/\/+$/,"");
            files.forEach(function(file) {
                var curPath = path + "/" + file;
                fs.lstat(curPath, function(err, stats) {
                    if( err ) {
                        callback(err, []);
                        return;
                    }
                    if( stats.isDirectory() ) {
                        _self.rmdirAsync(curPath, folderDone);
                    } else {
                        fs.unlink(curPath, folderDone);
                    }
                });
            });
        });
    }

    /**
     *
     */
    optimize() {
        var _self = this
        this.copyToTmp(function(results){
            _self.tmp_files = results
            _self.extractArchives(function(total){
                _self.archives = total
                _self.optimizeDir(_self.tmp_dir, function(){
                    //_self.generateArchive(function(file){
                    _self.archiveExtractedFiles(function(){
                        _self.success_callback(_self.tmp_dir)
                    })
                    //})
                })
            })
        }, function(err){
            _self.error_callback(err)
        })
    }

    /**
     *
     * @param startPath
     * @param filters
     */
    fromDir(startPath, filters){
        var _self = this;
        filters.map(function(filter){
            var files=fs.readdirSync(startPath);
            for(var i=0;i<files.length;i++){
                var f_name = filter.replace('.', '')
                var filename=path.join(startPath,files[i]);
                var stat = fs.lstatSync(filename);
                if (stat.isDirectory()){
                    _self.fromDir(filename,[filter]); //recurse
                }
                else if (filename.indexOf(filter)>=0) {
                    _self.op_files.push({
                        type:f_name,
                        path: filename
                    })
                }
            }
        })
    }

    /**
     *
     * @param dir
     * @returns {Array}
     */
    getOptimizingFiles(dir){
        this.fromDir(dir, [
            ".css",
            ".js",
            ".html",
            ".png",
            ".jpg",
            ".jpeg",
            ".CSS",
            ".JS",
            ".HTML",
            ".PNG",
            ".JPG",
            ".JPEG"
        ])
        return this.op_files
    }

    /**
     *
     * @param dir
     */
    optimizeDir(dir, sc) {
        var files = this.getOptimizingFiles(dir)
        var count = files.length
        var optimized = 0
        var _self = this
        if(count == 0){
            sc()
            return
        }
        files.map(function(el){
            _self.optimizeFile(el, function(){
                optimized++
                console.log(optimized)
                console.log(count)
                console.log("---------------")
                if(optimized == count){
                    sc()
                }
            })
        })
    }


    optimizeFile(file, success_callback){
        var type = file.type;
        type = type.toLowerCase();
        var el_path = file.path;
        if(type == 'js') {

            minify(el_path, function(error, data) {
                if(error || data == 'undefined' || data == undefined) {
                    success_callback()
                }else {
                    fs.unlink(el_path, function(){
                        fs.writeFile(el_path, data, 'utf8', function(){
                            success_callback()
                        });
                    })
                }
            });

        }
        if(type == 'css') {

            minify(el_path, function(error, data) {
                if(error || data == 'undefined' || data == undefined) {
                    success_callback()
                }else {
                    fs.unlink(el_path, function(){
                        fs.writeFile(el_path, data, 'utf8', function(){
                            success_callback()
                        });
                    })
                }

            });

        }

        if(type == 'html'){

            minify(el_path, function(error, data) {
                if(error || data == 'undefined' || data == undefined) {
                    success_callback()
                }else {
                    fs.unlink(el_path, function(){
                        fs.writeFile(el_path, data, 'utf8', function(){
                            success_callback()
                        });
                    })
                }

            });

        }

        if(type == 'png'){
            var new_path = el_path.replace(path.basename(el_path), "")
            imagemin([el_path], new_path, {
                plugins: [
                    imageminPngcrush()
                ]
            }).then(() => {
                success_callback()
            });
        }

        if(type == 'jpg' || type == "jpeg"){
            var new_path = el_path.replace(path.basename(el_path), "")
            imagemin([el_path], new_path, {
                plugins: [
                    imageminJpegtran()
                ]
            }).then(() => {
                success_callback()
            });
        }

    }

    /**
     * Check if file is archive
     * @param filename
     * @returns {*}
     */
    isArchive(filename){
        var archives = ["zip", "rar", "sz", "tar", "gz"]
        var fileType = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename) : ['folder'];
        fileType = (fileType[0] != "7z") ? fileType[0] : "sz"
        if(archives.indexOf(fileType) >= 0) {
            return fileType
        }
        return false
    }

    /**
     * Extract archives to folder
     * @param success_callback
     */
    extractArchives(success_callback){
        var _self = this
        var archives = []
        this.tmp_files.map(function(files){
            files.map(function(file){
                var file_path = file.dest;
                var archive = _self.isArchive(file_path)
                if(archive != false){
                    archives.push({
                        type: archive,
                        path: file_path
                    })
                }
            })
        })
        var count = archives.length
        var extracted = 0
        var success = []
        if(count == 0){
            success_callback(success)
            return
        }
        archives.map(function(el, idx){
            var el_path = el.path
            var el_type = el.type
            var replace = (el_type == 'sz') ? "7z" : el_type
            var el_folder = el_path.replace("."+replace, "")+'_'+el_type
            el_folder = el_folder.replace(".tar", "")

            fs.exists(el_folder, function(exists) {
                if (exists) {
                    el_folder = el_folder+'_'+idx
                }
                if(el_type == "zip"){
                    var zip = new AdmZip(el_path);
                    zip.extractAllToAsync(el_folder, true, function(){
                        fs.unlink(el_path, function(err){
                            extracted++;
                            success.push(el_folder)
                            if(extracted == count ){
                                success_callback(success)
                            }
                        })
                    });
                }
                if(el_type == "rar"){
                    var rar = new Unrar(el_path);
                    rar.extract(el_folder, null, function (err) {
                        fs.unlink(el_path, function(err){
                            extracted++;
                            success.push(el_folder)
                            if(extracted == count ){
                                success_callback(success)
                            }
                        })
                    });
                }
                if(el_type == "sz"){
                    var szip = new SZip();
                    szip.extractFull(el_path, el_folder, {}).then(function () {
                        fs.unlink(el_path, function(err){
                            extracted++;
                            success.push(el_folder)
                            if(extracted == count ){
                                success_callback(success)
                            }
                        })
                    });
                }
                if(el_type == "tar" || el_type == "gz"){
                    tarball.extractTarball(el_path, el_folder, function(err){
                        fs.unlink(el_path, function(err){
                            extracted++;
                            success.push(el_folder)
                            if(extracted == count ){
                                success_callback(success)
                            }
                        })
                    })
                }
            });

        })
    }

    /**
     * Create dir in tmp folder
     * @returns {*}
     */
    createTmpDir(){
        let tmp_dir = app.getPath('temp')
        let filename = 'cp_'+crypto.randomBytes(4).readUInt32LE(0)+'optimize'
        let files_path = path.join(tmp_dir, filename)
        fs.mkdirSync(files_path)
        return files_path

    }

    /**
     * Copy files and folder to tmp dir
     * @param success_callback
     * @param error_callback
     */
    copyToTmp(success_callback, error_callback){
        var files_path = this.createTmpDir()
        this.tmp_dir = files_path
        var count = this.files.length
        var copied = 0
        var total = []
        var errors = []
        this.files.map(function(file){
            copy(file.path, path.join(files_path, file.name), function(error, results) {
                if (error) {
                    copied++;
                    errors.push(error)
                } else {
                    copied++;
                    total.push(results)
                }
                if(copied == count && errors.length == 0){
                    success_callback(total)
                }else if(copied == count && errors.length > 0)  {
                    error_callback(errors)
                }
            });

        })
    }

}

module.exports = Optimize