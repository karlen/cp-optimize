const fadeIn = {
    appear: function(node, done){
        node.style.display='none';
        $(node).fadeIn(done);
        return {
            stop:function(){
                $(node).stop(true);
            }
        };
    },
    enter: function(){
        this.appear.apply(this,arguments);
    },
    leave: function(node, done){
        node.style.display='';
        $(node).fadeOut(done);
        return {
            stop:function(){
                // jq will call done on finish
                $(node).stop(true);
            }
        };
    }
}

module.exports.fadeIn  = fadeIn