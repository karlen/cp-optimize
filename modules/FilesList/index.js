import Animate from 'rc-animate'
import {fadeIn} from 'modules/Animation'
import  Optimize from 'modules/Optimize'
const remote = window.require('electron').remote;
const fs = window.require('fs');
const shell = remote.getGlobal('shell');
const dialog = remote.getGlobal('dialog');
import { Button, Modal } from 'react-bootstrap'
var path = window.require("path");

require('jquery-mousewheel');
class FilesList extends React.Component {

    constructor(props){
        super(props);
        this.file;
        this.optimizer;
        this.state = {
            files: [],
            showModal: false
        };
    }

    componentDidMount() {
        this.attachEvents()
    }

    download(file){
        file = file
        this.file = file
        this.setState(function(old_state){
            old_state.showModal = true
            return old_state;
        })

    }


    closeModal(){
        this.reset()
    }

    reset(){
        var _self = this
        $('.drag_overlay').hide()
        $('#component_main .upload_icon img').css('visibility', 'visible')
        $('#component_main .upload_icon').css('background-image', "")
        this.file = null
        _self.setState(function(old_state){
            var new_state ={
                files: [],
                showModal: false
            }
            return new_state;
        })
        window.optimize_process = false;
    }

    showOptimizedFolder(){
        var _self = this;
        shell.openItem(_self.file)
        _self.reset()
    }

    downloadOptimized(){
        var _self = this
        var saveIn = dialog.showSaveDialog({
            defaultPath: path.basename(_self.file+'.zip')
        })

        if(saveIn){
            _self.setState(function(old_state){
                old_state.showModal = false;
                return old_state;
            })
            _self.optimizer.generateArchive(function(file){
                _self.move_to(file+'.zip', saveIn, function (err) {
                    if (err) return console.error(err)
                    console.log("success!")
                    _self.reset()
                })

            })
        }


    }

    move_to (oldPath, newPath, callback) {

        function copy_and_delete () {
            var readStream = fs.createReadStream(oldPath);
            var writeStream = fs.createWriteStream(newPath);

            readStream.on('error', callback);
            writeStream.on('error', callback);
            readStream.on('close',
                function () {
                    fs.unlink(oldPath, callback);
                }
            );

            readStream.pipe(writeStream);
        }

        fs.rename(oldPath, newPath,
            function (err) {
                if (err) {
                    if (err.code === 'EXDEV') {
                        copy_and_delete();
                    } else {
                        callback(err);
                    }
                    return;// << both cases (err/copy_and_delete)
                }
                callback();
            }
        );
    }



    optimize(){
        var files = this.state.files;
        var _self = this
        if(files.length > 0){
            window.optimize_process = true;
            $('.drag_overlay').show()
            $('#component_main .upload_icon img').css('visibility', 'hidden');
            $('#component_main .upload_icon').css('background-image', "url('./assets/img/loader.gif')");
            var optimizer = new Optimize(files, function(file){
                _self.download(file)
            })
            _self.optimizer = optimizer;
        }
    }

    removeItem(filepath){

        this.setState(function(old_state){
            var new_state ={
                files: []
            };
            new_state.files = [];
            old_state.files.map(function(el){
                if(el.path != filepath){
                    new_state.files.push(el)
                }
            });
            return new_state;
        })

    }

    isFileAdded(filepath) {
        var is_added = false
        this.state.files.map(function(el){
            if(el.path == filepath){
                is_added = true
            }
        });
        return is_added
    }

    addFiles(files){
        var _self = this;
        _self.setState(function(previousState){
            var state = previousState;
            var length = files.length;
            for(var i = 0; i<length; i++) {
                if(!_self.isFileAdded(files[i].path)){
                    state.files.push(files[i]);
                }
            }
            return state;
        });
    }

    attachEvents() {

        var _self = this;
        var dropZone = $('#component_main'),
            maxFileSize = 100000000;

        dropZone[0].ondragover = function() {
            if(window.optimize_process){
                return false;
            }
            $('#component_main').find('.upload_icon').addClass('scaled')
            return false;
        }

        dropZone[0].ondragleave = function() {
            if(window.optimize_process){
                return false;
            }
            $('#component_main').find('.upload_icon').removeClass('scaled')
            return false;
        }

        dropZone[0].ondrop = function(event) {
            if(window.optimize_process){
                return false;
            }
            event.preventDefault();
            var files = event.dataTransfer.files;
            $('#component_main').find('.upload_icon').removeClass('scaled')
            _self.addFiles(files)

        }
        $('#component_fileslist').mousewheel(function(event, delta) {
            this.scrollLeft -= (delta * 30);
            event.preventDefault();
        });

        $('.upload_button').on('click', function(){
            var file = document.getElementById('upload_file');
            if(!file || file.length <= 0){
                var file = document.createElement('input');
                file.id= "upload_file";
                file.type="file"
                file.multiple="multiple"
                document.head.appendChild(file)
            }
            file.click();

            file.onchange = function(){
                var files_list = $("#upload_file")[0].files;
                _self.addFiles(files_list)
            }

        })
    }

    render() {
        var _self = this;

        if (this.state.files.length > 0) {

            return (



                <div>


                    <Modal show={this.state.showModal} onHide={_self.closeModal.bind(_self)}>
                        <Modal.Header closeButton>
                            <Modal.Title>Optimization finished</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            <Button onClick={_self.downloadOptimized.bind(_self)}>Download optimized files</Button>
                            <hr></hr>
                            <Button onClick={_self.showOptimizedFolder.bind(_self)}>Open optimized files</Button>
                        </Modal.Body>
                    </Modal>

                    <div id="component_fileslist" className="files_exist">

                        <div className="files_list">
                            {
                                this.state.files.map(function(file, idx){
                                    var className = "preview ";
                                    var fileType = (/[.]/.exec(file.name)) ? /[^.]+$/.exec(file.name) : ['folder'];
                                    fileType = (fileType[0] != "7z") ? fileType[0] : "sz"
                                    className =  className + fileType;
                                    return (
                                        <Animate animation={fadeIn} key={idx}>
                                            <div className= "file">
                                                <div className={className}>

                                                </div>
                                                <div className="title">
                                                    {file.name}
                                                </div>
                                                <div className="remove" onClick={_self.removeItem.bind(_self, file.path)}>

                                                </div>
                                            </div>
                                        </Animate>
                                    )
                                })
                            }
                            <div className="clear"></div>
                        </div>
                    </div>
                    <div className="optimize_button" onClick={_self.optimize.bind(_self)} >OPTIMIZE</div>
                </div>

            )
        }else {
            return (

                <div>
                    <div id="component_fileslist"></div>
                    <div className="optimize_button" onClick={_self.optimize.bind(_self)} >OPTIMIZE</div>
                </div>

            )
        }

    }
}

module.exports = FilesList