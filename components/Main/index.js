import { Button } from 'react-bootstrap'
import Animate from 'rc-animate'
import {fadeIn} from 'modules/Animation'
import FilesList from 'modules/FilesList'
class Main extends React.Component {
    constructor(props){
        super(props);
        this.state = {

        };
    }

    componentDidMount() {
        this.attachEvents()
    }

    attachEvents() {

    }

    render() {
        return (
            <div id="component_main">
                <div className="wrap">
                    <Animate animation={fadeIn}>
                        <div className="container">
                            <div className="row">
                                <div className="main-wrapper col-lg-10 col-md-10 col-sm-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0">

                                    <div className="upload_icon col-lg-10 col-md-10 col-sm-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-0">
                                        <img  src="./assets/img/upload_small.png"/>
                                    </div>

                                    <div className="title">
                                        <h2>Drag & Drop  files</h2>
                                        <hr/>
                                    </div>

                                    <div className="upload_button col-lg-6 col-md-6 col-sm-6 col-lg-offset-3 col-md-offset-3 col-sm-offset-3">
                                        <Button block>or click this button to upload</Button>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </Animate>
                </div>
                <div className="drag_overlay"></div>
                <div className="files_list_wrap">
                    <FilesList></FilesList>
                </div>
            </div>
        );
    }
}

module.exports = Main