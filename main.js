const {app, Menu, Tray, nativeImage, BrowserWindow, ipcMain, shell, dialog} = require('electron')
const path = require('path')
let mainWindow = null

function createWindow () {
  let img = nativeImage.createFromPath(__dirname + '/favicon.png');
  mainWindow = new BrowserWindow({width: 650, height: 650, icon: img})
  mainWindow.loadURL(`file://${__dirname}/index.html`)
  //mainWindow.webContents.openDevTools()
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

app.on('ready', function(){
  createWindow()
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  if (mainWindow === null) {
    createWindow()
  }
})

global.app = app;
global.shell = shell;
global.dialog = dialog;
global.BrowserWindow = BrowserWindow;
